#![warn(clippy::all, clippy::pedantic)]
#![feature(peer_credentials_unix_socket)]

use std::env;
use std::io::{Read, Write};
use std::os::fd::AsRawFd;
use std::os::unix::{
    net::{UnixListener, UnixStream},
    ucred::peer_cred,
};
use std::path::Path;
use std::process;

use nix::sys::socket::{setsockopt, sockopt};

const SOCK: &str = "crew_sock";

fn main() -> anyhow::Result<()> {
    let role = env::args()
        .nth(1)
        .expect("Role must be 'client' or 'server'");

    let sock = Path::new(SOCK);

    match role.as_str() {
        "server" => {
            if sock.exists() {
                std::fs::remove_file(sock)?;
            }
            let listener = UnixListener::bind(sock)?;
            setsockopt(listener.as_raw_fd(), sockopt::PassCred, &true)?;

            println!("Server started. Listening...");

            let mut msg = String::new();
            for mut client in listener.incoming().flatten() {
                let cred = peer_cred(&client)?;
                client.read_to_string(&mut msg)?;
                println!("--\nClient: {cred:?}\n{msg}");
                msg.clear();
            }
        }
        "client" => {
            let mut stream = UnixStream::connect(sock)?;
            let cred = peer_cred(&stream)?;
            let pid = process::id();
            println!("Client with PID {pid} connected to Server {cred:?}.");

            stream.write_all(format!("Server: {cred:?}").as_bytes())?;
        }
        _ => {}
    }

    Ok(())
}
