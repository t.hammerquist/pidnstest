# pidnstest

A client/server program to peek at the UNIX domain socket peer credentials
(`SO_PEERCRED`) of a socket connection.

## Server

1. Creates a UNIX domain socket (`crew_sock`) in the CWD.
2. Binds to the socket.
3. Waits for a message from a client.
4. Collects the `UCred` struct representing the client's UID, GID, and PID.
5. Displays both `UCred` structs.
6. GOTO 3

## Client

1. Looks for the socket and connects to it.
2. Sends a message to the server which contains a string representation of its
    `UCred` struct - the server's UID, GID, and PID as reported by the kernel.
3. Exits.

# Usage

In terminal A:

```
open@thammerquist-crux:~/code/pidnstest$ make server
cargo clean
docker run -u $UID -it --rm -v $PWD:/app -w /app rustlang/rust:nightly-alpine \
        sh -c 'CARGO_HOME="./.cargo" cargo build --release'
    Updating crates.io index
    ...
   Compiling pidnstest v0.1.0 (/app)
    Finished release [optimized] target(s) in 1m 07s
cp target/*/pidnstest ./driver
./driver server
Server started. Listening...
```

In terminal B:

```
$ make client

*** Sending from host...
./driver client
Client with PID 54182 connected to Server UCred { uid: 1000, gid: 1000, pid: Some(54137) }.

*** Sending from container as root...
docker run -it --rm -v $PWD:/app -w /app archlinux ./driver client
Client with PID 1 connected to Server UCred { uid: 0, gid: 0, pid: Some(0) }.

*** Sending from privileged container as root...
docker run --privileged -it --rm -v $PWD:/app -w /app archlinux ./driver client
Client with PID 1 connected to Server UCred { uid: 0, gid: 0, pid: Some(0) }.

*** Sending from container as non-root...
docker run -u 1000 -it --rm -v $PWD:/app -w /app archlinux ./driver client
Client with PID 1 connected to Server UCred { uid: 0, gid: 0, pid: Some(0) }.

*** Sending from privileged container as non-root...
docker run --privileged -u 1000 -it --rm -v $PWD:/app -w /app archlinux ./driver client
```

Meanwhile, output similar to the following should appear back in terminal A:

```
Server started. Listening...
--
Client: UCred { uid: 1000, gid: 1000, pid: Some(54182) }
Server: UCred { uid: 1000, gid: 1000, pid: Some(54137) }
--
Client: UCred { uid: 1000, gid: 1000, pid: Some(54234) }
Server: UCred { uid: 0, gid: 0, pid: Some(0) }
--
Client: UCred { uid: 1000, gid: 1000, pid: Some(54331) }
Server: UCred { uid: 0, gid: 0, pid: Some(0) }
--
Client: UCred { uid: 100999, gid: 1000, pid: Some(54428) }
Server: UCred { uid: 0, gid: 0, pid: Some(0) }
--
Client: UCred { uid: 100999, gid: 1000, pid: Some(54523) }
Server: UCred { uid: 0, gid: 0, pid: Some(0) }
```

# Notes

Several things of note:

- The client's credentials given to the server always have a non-zero PID.
- The server's credentials given to the client are only correct when 
- The PID in the any peer's is only correct in when both endpoints are on the host.
- The PID for a containerized client's credentials do not map to a PID within the container.
- The PID for the host's credentials is always 0 when the client is in a container.

# Links

- [unix(7)](https://man7.org/linux/man-pages/man7/unix.7.html)
- [pid_namespaces(7)](https://man7.org/linux/man-pages/man7/pid_namespaces.7.html)
