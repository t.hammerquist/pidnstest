# Uncommend to build the release binary
RELEASE := --release

SOCKNAME := crew_sock

DOCKER_OPTS := -it --rm -v $$PWD:/app -w /app
DOCKER_BUILD_IMG := rustlang/rust:nightly-alpine
DOCKER_RUN_IMG := archlinux

CLIENT_TARGETS := client_host \
									client_root \
									client_root_priv \
									client_nonroot \
									client_nonroot_priv

.PHONY: build clean server client $(CLIENT_TARGETS)\

all:
	@echo "Targets:"
	@echo "  make driver    => build the driver for both client and server"
	@echo "  make server    => launch the server and start listening"
	@echo "  make client    => run client in each successive enviroment"
	@echo "  make clean     => clean all build products and cache"
	@echo
	@echo "Note that the server and client should be run in separate terminals."

build: driver

# This recipe builds the binary in an alpine container to ensure libc (musl) is
# statically linked, allowing it to be run in any Linux base image.
#
# We set CARGO_HOME so that the crate index is shared via the same bind mount
# used to retain the build products. This prevents having to re-fetch the index
# every build.
driver: Cargo.* src/*
	cargo clean
	docker run $(DOCKER_OPTS) $(DOCKER_BUILD_IMG) \
		sh -c 'CARGO_HOME="./.cargo" cargo build $(RELEASE)'
	cp target/*/pidnstest ./driver

server: driver
	./driver server

client: $(CLIENT_TARGETS)

client_host: driver
	@echo "\n*** Sending from host..."
	./driver client

client_root: driver
	@echo "\n*** Sending from container as root..."
	docker run $(DOCKER_OPTS) $(DOCKER_RUN_IMG) ./driver client

client_root_priv: driver
	@echo "\n*** Sending from privileged container as root..."
	docker run --privileged $(DOCKER_OPTS) $(DOCKER_RUN_IMG) ./driver client

client_nonroot: driver
	@echo "\n*** Sending from container as non-root..."
	docker run -u 1000 $(DOCKER_OPTS) $(DOCKER_RUN_IMG) ./driver client

client_nonroot_priv: driver
	@echo "\n*** Sending from privileged container as non-root..."
	docker run --privileged -u 1000 $(DOCKER_OPTS) $(DOCKER_RUN_IMG) ./driver client

clean:
	rm -f driver $(SOCKNAME)
	rm -r target

distclean: clean
	if [ -d ./.cargo ]; then rm -rf ./.cargo; fi
